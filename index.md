---
layout: home
title: Hello world
Description: >
  Hello world example of Gitlab Pages on salsa.debian.org.
---

Hello world!

Check out the sample posts below. They reside in the [\_posts/][postdir]
directory.

This is a C source of the famous _Hello world_ example. It is using
highlighting.

```c
#include <stdio.h>
#include <stdlib.h>

int main() {
  printf("Hello World\n");
  exit(0);
}
```

[postdir]: https://salsa.debian.org/dleidert/jekyll-minima/blob/master/_posts/

## Heading 2

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
eos et accusam et

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
