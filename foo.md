---
layout: page
title: Foo
Description: >
  This page is shown in the header.
---

This page is shown in the page header, because it is listed in the
`header_pages:` key in *_config.yml*.

The following markup is just an example of how code is rendered.

{% highlight c linenos %}
#include <stdio.h>
#include <stdlib.h>

int main() {
  printf("Hello World\n");
  exit(0);
}
{% endhighlight %}

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
