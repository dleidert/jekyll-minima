---
layout: page
title: Foo
Description: >
  This page is shown in the header.
---

This page is not shown in the page header, because it is not listed in the
`header_pages:` key in *_config.yml*.

This is a different highlighting approach.

{% highlight c linenos=table %}
#include <stdio.h>
#include <stdlib.h>

int main() {
  printf("Hello World\n");
  exit(0);
}
{% endhighlight %}


<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
