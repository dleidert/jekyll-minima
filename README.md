# Example for using Jekyll and Gitlab Pages on salsa.debian.org

This project demonstrates how to easily setup a static website on
[salsa.debian.org](https://salsa.debian.org) using
[Jekyll](https://jekyllrb.com/) and the [Gitlab
Pages](https://salsa.debian.org/help/user/project/pages/index.md) feature.

It is not necessary to clone the
[pages/jekyll](https://gitlab.com/pages/jekyll) example, which contains all the
files belonging to the theme too. Copying them into your pages project is not
necessary (and in my opinion also not useful). Instead the pages project can be
easily set up by using just a few files:

```
.gitlab-ci.yml
_config.yml
Gemfile
```

That is all you need to setup a static website. *.gitattributes* and
*.gitignore* are not necessary too, but serve some purpose. You can decide, if
you use them.

## Getting Started

Fork this project and adjust *_config.yml* and - if necessary - *Gemfile*.

The file *_config.yml* holds the basic configuartion of your website, like the
title, description, theme, author, basic URL and some theme relevant
configurations. It is the place to declare plugins, collections, permalinks,
etc.

*Gemfile* contains a list of all the Ruby gems required to build your website
using the `pages` job defined in *.gitlab-ci.yml*.

The file *.gitlab-ci.yml* defines the job used to automatically build and (in
case of **master**) also deploy the pages as soon as you push your changes to
the Git repository.

### *_config.yml*

Lets explain this file line by line.

#### Theme

This example uses the [`minima`](https://github.com/jekyll/minima) theme.

```yaml
theme: minima
```
{: title="theme field in \_config.yml"}

Of course there are a lot more (stable) themes available as Ruby gem. In this
case add them to *Gemfile* and change the `theme` key. If you want to use a
remote theme, then the
[`jekyll-remote-theme`](https://github.com/benbalter/jekyll-remote-theme)
plugin might be of assistance.

##### Theme Controls

Some themes allow to customize their appearance or add social data. In case of
the `minima` theme, social links can be created by adding the social media
usernames. In this case links to Debian's social media sites are created:

```yaml
rss: rss
twitter_username: debian
facebook_username: debian
linkedin_username: debian
flickr_username: debian
```
{: title="social media user names in \_config.yml"}

#### Title and Description

Choose some descriptive title and a meaningful description of the site. This
information will be added to the websites head meta data and shown at the site.

```yaml
title: Minimal Example using Gitlab Pages and Minima
description: >
  This is a proof-of-concept project and shows how to use the Gitlab Pages
  feature of salsa.debian.org creating a website based on Jekyll and the Minima
  theme.
```
{: title="title and description field in \_config.yml"}

#### Author

The site can also have author information, often shown at the bottom of the
page.

```yaml
author: Daniel Leidert
email: dleidert@debian.org
```

#### URL and Base URL of User, Group and Project Pages

For a user page a project named `USER.pages.debian.net` must be created in
`https://salsa.debian.org/USER`. The resulting website URL will be
`**USER**.pages.debian.net`.

The user page does not have a base URL, so only the root URL should be added to
*_config.yml*. If this is not done, some plugins like
[`jekyll-sitemap`](https://github.com/jekyll/jekyll-sitemap) might not work
correctly .

```yaml
url: https://USER.pages.debian.net
```

Group pages are similar. The project should be named `GROUP.pages.debian.net`
in the groups main namespace `https://salsa.debian.org/GROUP`. Also configuring
the root URL is enough:

```yaml
url: https://GROUP.pages.debian.net
```

Project pages are a bit different. As soon as they have been deployed, they can
be found at `USER.pages.debian.net/PROJECT` or
`GROUP.pages.debian.net/PROJECT`, depending of where the project is hosted - in
the user or the group namespace. Because of this the `baseurl` key must be
configured together with the `url` key:

```yaml
url: https://USER.pages.debian.net
baseurl: /PROJECT
```

#### Plugins

...

### _Gemfile_

*Gemfile* contains a list of all the Ruby gems required to build your website
using the `pages` job defined in *.gitlab-ci.yml*. Feel free to add the  gems
you like or change the version requirements of the gems listed  there. Note,
that this file only handles the installation of a Jekyll plugin gem. To enable
a plugin for Jekyll it must also be listed in the `plugins:` section of
*_config.yml*. Some useful plugins might be:

* [jekyll-data](https://github.com/ashmaroli/jekyll-data)



## Documentation

* Jekyll: https://jekyllrb.com/
* Liquid: https://shopify.github.io/liquid/

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
